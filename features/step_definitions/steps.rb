Given(/^El usuario se encuentra en la pagina de tinet$/) do
  visit "http://www.tinet.cl"
end

Given(/^ingresa al link de contacto$/) do
  click_link('Contacto')
  #sleep 5 #lo mando a dormir, ya que no alcanza a cargar el formulario (OJO CUANDO NO LO ENCUENTRA)
end

When(/^El usuario ingresa "([^"]*)"$/) do |nombre|
  fill_in('your-name', with: nombre)
end

When(/^ingresa correo "([^"]*)"$/) do |correo|
  fill_in('your-email', with: correo)
end

When(/^ingresa asunto "([^"]*)"$/) do |asunto|
  fill_in('your-subject', with: asunto)
end

When(/^ingresa mensaje "([^"]*)"$/) do |mensaje|
  fill_in('your-message', with: mensaje)
end

When(/^ingresa captcha "([^"]*)"$/) do |captcha|
    fill_in('captcha-333', with: captcha)
end

When(/^presiona el boton enviar$/) do
    click_button('Enviar')
  #sleep 5
end

Then(/^El codigo captcha es incorrecto$/) do
  a = find(:css, "span.wpcf7-form-control-wrap:nth-child(3) > span:nth-child(2)").text
  expect(a).to eq("El código ingresado es incorrecto.")
end
