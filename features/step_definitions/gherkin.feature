
@tagall
Feature: Formulario tinet

@scenario1
Scenario: Completar el formulario un codigo catcha incorrecto
  Given El usuario se encuentra en la pagina de tinet
    And ingresa al link de contacto
  When El usuario ingresa "Sebastian"
    And ingresa correo "acevedoc.sebastian@gmail.com"
    And ingresa asunto "probando"
    And ingresa mensaje "probando selenium on cucumber"
    And ingresa captcha "123"
    And presiona el boton enviar
  Then El codigo captcha es incorrecto

@scenario2
Scenario Outline: Completar el formulario con parametros
  Given El usuario se encuentra en la pagina de tinet
    And ingresa al link de contacto
  When El usuario ingresa "<nombre>"
    And ingresa correo "<correo>"
    And ingresa asunto "<asunto>"
    And ingresa mensaje "<mensaje>"
    And ingresa captcha "<captcha>"
    And presiona el boton enviar
  Then El codigo captcha es incorrecto

  Examples:
    |nombre   |correo                      |asunto   |mensaje  |captcha|
    |sebastian|acevedoc.sebastian@gmail.com|probando |probando |asd    |
    |dania    |dania.montanares@usach.cl   |probando2|probando2|asds   |
