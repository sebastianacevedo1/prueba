require "cucumber"
require "selenium-webdriver"
require "rspec/expectations"
require "pry"
require "capybara/cucumber"
require "capybara-screenshot/cucumber"
#require 'capybara/dsl'

include RSpec::Matchers
include Capybara::DSL

Capybara.default_driver = :selenium
Capybara.default_max_wait_time = 5 #segundos
